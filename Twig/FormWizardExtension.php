<?php

namespace CMS\FormWizardBundle\Twig;

use CMS\FormWizardBundle\Exception\InvalidArgumentException;
use CMS\FormWizardBundle\Wizard;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig_Environment;

class FormWizardExtension extends \Twig_Extension
{
    /**
     * @var  ContainerInterface
     */
    private $container;

    /**
     * FormWizardExtension constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('form_wizard', [$this, 'formWizard'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('form_wizard_steps', [$this, 'formWizardSteps'], ['is_safe' => ['html'], 'needs_environment' => true])
        ];
    }

    public function formWizard(Twig_Environment $environment, $wizardName, $step = null, $template = 'FormWizardBundle:Twig:wizard.html.twig')
    {
        if (!$this->container->has('cms.form_wizard.' . $wizardName)) {
            throw new InvalidArgumentException(sprintf('wizard %s not found!', $wizardName));
        }

        /**
         * @var Wizard $wizard
         */
        $wizard = $this->container->get('cms.form_wizard.' . $wizardName);

        $form = $wizard->getStepForm($step);

        /** @var Request $request */
        $request = Request::createFromGlobals();

        if ($request->isMethod($wizard->getStepMethod($step))) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $wizard->flush($step, $form->getData());
            }
        }

        return $environment->render($template, [
            'form' => $form->createView()
        ]);
    }

    public function formWizardSteps(Twig_Environment $environment, $wizardName, $currentStep = null, $template = 'FormWizardBundle:Twig:wizard_steps.html.twig')
    {
        if (!$this->container->has('cms.form_wizard.' . $wizardName)) {
            throw new InvalidArgumentException(sprintf('wizard %s not found!', $wizardName));
        }

        /**
         * @var Wizard $wizard
         */
        $wizard = $this->container->get('cms.form_wizard.' . $wizardName);

        return $environment->render($template, [
            'steps' => $wizard->getSteps(),
            'currentStep' => $currentStep
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'form_wizard';
    }
}
