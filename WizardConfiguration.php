<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 22.10.15
 * Time: 15:14
 */

namespace CMS\FormWizardBundle;


use Symfony\Component\Form\FormFactory;

class WizardConfiguration
{
    private $steps = [];


    /**
     * WizardConfiguration constructor.
     * @param array $steps
     * @param FormFactory $formFactory
     */
    public function __construct(array $steps, FormFactory $formFactory)
    {
        $index = 0;
        $countSteps = count($steps);
        foreach ($steps as $name => $properties) {
            $this->steps[$name] = new WizardStep(
                $name,
                $properties['type'],
                $properties['condition'],
                $properties['template'],
                $formFactory
            );

            $this->steps[$name]
                ->setIslast($index)
                ->setIsFirst($index == 0)
                ->setIsLast($index == ($countSteps - 1));

            $index++;
        }
    }

    /**
     * @return array
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param $name
     * @return WizardStep
     */
    public function getStep($name)
    {
        if (isset($this->steps[$name])) {
            return $this->steps[$name];
        }

        return null;
    }

    /**
     * @return WizardStep
     */
    public function getFirstStep()
    {
        return reset($this->steps);
    }

    /**
     * @return mixed
     */
    public function getLastStep()
    {
        return end($this->steps);
    }

    /**
     * @param null $currentStepName
     * @return WizardStep|mixed
     */
    public function getPrevStep($currentStepName = null)
    {
        $stepNames = array_keys($this->steps);

        $indexCurrent = array_search($currentStepName, $stepNames);

        return $this->steps[$indexCurrent >= 1 ? $stepNames[$indexCurrent - 1] : $stepNames[0]];
    }

    /**
     * @param null $currentStepName
     * @return WizardStep|null
     */
    public function getNextStep($currentStepName = null)
    {
        $currentStep = null === $currentStepName ? $this->getFirstStep() : $this->getStep($currentStepName);

        $nextStep = false;

        foreach ($this->steps as $name => $step) {
            if ($name == $currentStep->getName()) {
                $nextStep = current($this->steps);
                break;
            }
        }

        return $nextStep;
    }

    /**
     * @param array $steps
     * @return $this
     */
    public function setSteps($steps)
    {
        $this->steps = $steps;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return md5(serialize($this));
    }
}